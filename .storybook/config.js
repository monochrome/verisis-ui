import { addDecorator, configure } from '@storybook/react';
import { setIntlConfig, withIntl } from 'storybook-addon-intl';
import { withKnobs } from '@storybook/addon-knobs';

// Load the locale data for all your defined locales
import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';
import arLocaleData from 'react-intl/locale-data/ar';

addLocaleData(enLocaleData);
addLocaleData(arLocaleData);

// Provide your messages
const messages = {
  en: { 'button.label': 'Click me!' },
  ar: { 'button.label': 'Klick mich!' }
};

const getMessages = (locale) => messages[locale];

// Set intl configuration
setIntlConfig({
  locales: ['en', 'ar'],
  defaultLocale: 'en',
  getMessages
});

// Register decorator
addDecorator(withIntl);
addDecorator(withKnobs);

function loadStories() {
  require('../src/stories');
}

configure(loadStories, module);
