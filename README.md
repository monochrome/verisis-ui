# Verisis UI Elements
## Description
Specialized/modified [Material UI](https://material-ui.com) components that used for Verisis projects.
## Installation
Verisis UI is available as an [npm package](https://www.npmjs.com/package/verisis-ui).
```
// with npm
npm install @material-ui/core @material-ui/icons @material-ui/pickers
npm install verisis-ui

// with yarn
yarn add @material-ui/core @material-ui/icons @material-ui/pickers
yarn add verisis-ui
```
## Usage
Here is a quick example to get you started, **it's all you need**:
```js
import React from 'react';
import ReactDOM from 'react-dom';
import { VrsTextField } from 'verisis-ui';

function App() {
  return (
    <VrsTextField label="What is your name?" />
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```
## Documentation
There is no documentation at this moment. But you can see components with `yarn start` after cloning this repository. Do not forget to make `yarn install` of course. We create a storybook to guide us.

## Licence
This project is licensed under the terms of the [MIT license](https://gitlab.com/monochrome/verisis-ui/blob/master/LICENCE).
