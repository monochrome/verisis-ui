import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { boolean, text, color, number, object, select } from '@storybook/addon-knobs';
import { Edit, KeyboardArrowDown } from '@material-ui/icons';
import '../components/styles/global.scss';
import '../components/styles/global-reboot.scss';

import Alert from '../components/alert/Alert';
import VrsButton from '../components/buttons/Buttons';
import FabButton from '../components/buttons/FabButton';
import SubButton from '../components/buttons/SubButton';
import Chart from '../components/charts/Chart';
import barChartData from './barChartData.json';
import pieChartData from './pieChartData.json';
import tableData from './tableData.json';
import VrsDialog from '../components/dialog/VrsDialog';
import ModalButton from '../components/buttons/ModalButton';
import Dropdown from '../components/dropdown/Dropdown';
import DropdownMenu from '../components/dropdown/DropdownMenu';
import DropdownMenuItem from '../components/dropdown/DropdownMenuItem';
import me from './0.jpeg';
import VrsCheckbox from '../components/form-items/VrsCheckbox';
import VrsDatePicker from '../components/form-items/VrsDatePicker';
import VrsSelectField from '../components/form-items/VrsSelectField';
import { InlineRadio } from '../components/form-items/VrsRadio';
import VrsSwitch from '../components/form-items/VrsSwitch';
import VrsTextField from '../components/form-items/VrsTextField';
import {
  DnaIcon,
  ProfileIcon,
  TestTube,
  TestTubeAdd,
  ClimsLogo,
  ClimsColorful,
  ExcelIcon,
  Export
} from '../components/icons';
import BaseList from '../components/list/BaseList';
import VrsPatternLock from '../components/mobile-pattern/VrsPatternLock';
import Popup from '../components/popup/Popup';
import SearchBox from '../components/form-items/SearchBox';
import VrsSnackbar from '../components/snackbar/VrsSnackbar';
import WebCamDialog from '../components/webcam/WebCamDialog';
import FilterMenu from '../components/filter/FilterMenu';
import FilterContentExpertise from './FilterContentExpertise';

storiesOf('Alert', module).add('Default', () => (
  <Alert
    alertTitle={text('Title', 'You opened title!')}
    alertText={text('Helper Text', 'You are doing something unexpected. Are you sure to continue?')}
    onCancelClick={action('cancelButton')}
    cancelButtonText={text('Cancel Button Text', 'Cancel')}
    onContinueClick={action('continueButton')}
    continueButtonText={text('Continue Button Text', 'Continue')}
    open={boolean('Is Alert Open', true)}
    onClick={action('clicked')}
  />
));

storiesOf('Buttons', module)
  .add('Default', () => (
    <VrsButton
      hasBg={boolean('Has Background?', false)}
      danger={boolean('Danger?', false)}
      fullWidth={boolean('Full Width?', false)}
      outlined={boolean('Outlined?', false)}
    >
      {text('Content', 'Click Here')}
    </VrsButton>
  ))
  .add('Default With Icon', () => (
    <VrsButton
      hasBg={boolean('Has Background?', false)}
      danger={boolean('Danger?', false)}
      fullWidth={boolean('Full Width?', false)}
      outlined={boolean('Outlined?', false)}
      icon={<KeyboardArrowDown />}
    >
      {text('Content', 'Click Here')}
    </VrsButton>
  ))
  .add('Fab Button', () => (
    <FabButton
      disabled={boolean('Disabled?', false)}
      danger={boolean('Danger?', false)}
      background={color('Background Color', '#000')}
      icon={<KeyboardArrowDown />}
      title={text('Tooltip', 'Hey! This is a tooltip.')}
      onClick={action('button click')}
    />
  ))
  .add('Button with Dropdown', () => {
    const item = {
      name: 'action_expertise_list_export',
      dropdown: [
        { name: 'Print', action: action('clickPrint') },
        { name: 'Export to Excel', action: action('clickExcel') }
      ]
    };

    return (
      <SubButton
        handleClick={action('button click')}
        dropdown={boolean('Dropdown?', true)}
        icon={<KeyboardArrowDown />}
        item={item}
        label={text('Label', 'Click Here')}
      />
    );
  });

storiesOf('Charts', module)
  .add('Bar Chart', () => (
    <Chart chartHeight={number('Chart Height', 400)} type="bar" data={object('Data', barChartData)} />
  ))
  .add('Pie Chart', () => (
    <Chart chartHeight={number('Chart Height', 400)} type="pie" data={object('Data', pieChartData)} />
  ));

storiesOf('Dialog', module).add('Default Dialog', () => (
  <VrsDialog
    open={boolean('Open?', true)}
    title={text('Title', 'Default Dialog Title')}
    onClose={action('onClose')}
    maxWidth={select('Width', ['sm', 'md', 'lg', 'xs'], 'sm')}
    dialogActions={
      <>
        <ModalButton label={text('ModalButtonText', 'This is modal button')} />
      </>
    }
  >
    This is children. You can add here everything like component, form or anything.
  </VrsDialog>
));

storiesOf('Dropdown', module).add('Dropdown', () => (
  <Dropdown image={me} icon={<KeyboardArrowDown />} hideArrow={boolean('Hide Arrow', false)}>
    You can add children or DropdownMenu and DropdownMenuItem.
    <DropdownMenu>
      <DropdownMenuItem itemName={text('First Dropdown Menu Item Name', 'Profile')} />
      <DropdownMenuItem danger={boolean('Danger', false)} itemName={text('Second Dropdown Menu Item Name', 'Logout')} />
    </DropdownMenu>
  </Dropdown>
));

storiesOf('Form Fields', module)
  .add('Text Field', () => (
    <VrsTextField
      label={text('Label', 'Hello World! What is your name?')}
      placeholder={text('Placeholder')}
      disabled={boolean('Disabled?', false)}
      multiline={boolean('Multiline?', false)}
      variant={select('Variant', ['standard', 'outlined', 'filled'], 'standard')}
    />
  ))
  .add('Select Field', () => {
    const data = [
      { value: 'ocean', label: 'Ocean', isFixed: true },
      { value: 'blue', label: 'Blue', color: '#0052CC', isDisabled: true },
      { value: 'purple', label: 'Purple' },
      { value: 'red', label: 'Red', isFixed: true },
      { value: 'orange', label: 'Orange' },
      { value: 'yellow', label: 'Yellow' },
      { value: 'green', label: 'Green' },
      { value: 'forest', label: 'Forest' },
      { value: 'slate', label: 'Slate' },
      { value: 'silver', label: 'Silver' }
    ];

    return (
      <VrsSelectField
        isCreatable={boolean('Is Creatable?', false)}
        label={text('Label', 'Countries')}
        onChange={action('onChange')}
        disabled={boolean('Disabled?', false)}
        isMulti={boolean('Multi Select?', false)}
        data={object('Options', data)}
      />
    );
  })
  .add('Date Picker', () => (
    <VrsDatePicker
      clearable={boolean('Clearable?', true)}
      showTodayButton={boolean('Show today button?', true)}
      disabled={boolean('Disabled?', false)}
      label={text('Label', 'When was @mustaphaturhan born?')}
      datetime={boolean('Is date time?', false)}
    />
  ))
  .add('Checkbox', () => (
    <VrsCheckbox
      isRight={boolean('Side is right?', false)}
      label={text('Label', 'Am I a checkbox?')}
      value={boolean('Checked?', false)}
      onChange={action('onChange')}
      inline={boolean('Inline?', false)}
      radial={boolean('Radial?', false)}
    />
  ))
  .add('Radio', () => (
    <InlineRadio
      label={text('Label', 'Do you think I am OK?')}
      value={1}
      name="deneme"
      placement={select('Label Placement', ['top', 'start', 'bottom', 'end'], 'end')}
    />
  ))
  .add('Switch', () => <VrsSwitch label={text('Label', 'Show all')} onChange={action('changeSwitch')} />)
  .add('Two Side Switch', () => {
    const label = {
      left: 'Light Mode',
      right: 'Dark Mode'
    };
    return <VrsSwitch twoSide label={object('Label', label)} onChange={action('changeSwitch')} />;
  })
  .add('Search Box', () => <SearchBox inline={boolean('Inline?', false)} />);

storiesOf('Icons', module).add('Icons', () => (
  <ul>
    <li>
      <DnaIcon /> / DnaIcon
    </li>
    <li>
      <ProfileIcon /> / ProfileIcon
    </li>
    <li>
      <TestTube /> / TestTube
    </li>
    <li>
      <TestTubeAdd /> / TestTubeAdd
    </li>
    <li>
      <ClimsLogo /> / ClimsLogo
    </li>
    <li>
      <ClimsColorful /> / ClimsColorful
    </li>
    <li>
      <ExcelIcon /> / ExcelIcon
    </li>
    <li>
      <Export /> / Export
    </li>
  </ul>
));

const columns = [
  { accessor: 'caseNumber', Header: 'Case Number', width: 100 },
  { accessor: 'senderInstituteCaseNo', Header: 'Sender Institute Case Number', width: 130 },
  { accessor: 'openningDate', Header: 'Opening Date', width: 110 },
  { accessor: 'closingDate', Header: 'Closing Date', width: 110 },
  { accessor: 'caseSubject', Header: 'Subject' },
  { accessor: 'senderInstitute', Header: 'Sender Institute', width: 200 },
  { accessor: 'evidencesCount', Header: 'Evidence Count', width: 100 },
  { accessor: 'expertiseStatus', Header: 'Expertise Status', width: 200 }
];

const actionList = [
  {
    icon: <Edit />,
    action: action('tableAction'),
    title: 'Edit'
  }
];

storiesOf('Tables', module)
  .add('Base List', () => <BaseList columns={columns} data={tableData} tableActions={actionList} />)
  .add('Base List with Checkbox', () => (
    <BaseList
      keyField="caseNumber"
      checkbox
      columns={columns}
      selection={[]}
      data={tableData}
      tableActions={actionList}
    />
  ));

storiesOf('Mobile Pattern', module).add('Pattern Lock', () => (
  <VrsPatternLock
    width={500}
    size={number('size', 3)}
    disabled={boolean('Disabled?', false)}
    freeze={boolean('Freeze?', true)}
    onChange={action('changePattern')}
  />
));

storiesOf('Notifications', module)
  .add('Popup', () => (
    <div style={{ position: 'relative' }}>
      <p>
        There should be position relatived parent element. For example, we are using div with position:relative in here.
      </p>
      <Popup open={boolean('Open?', true)}>I am a content.</Popup>
    </div>
  ))
  .add('Snackbar', () => (
    <VrsSnackbar
      open
      message={text('Message', 'I am a snackbar message.')}
      onClose={action('closing')}
      action={text('Action Message')}
      variant={select('Variant', ['danger', 'warning', 'success', 'none'], 'none')}
    />
  ));

storiesOf('Webcam', module).add('Webcam', () => <WebCamDialog onClose={action('close')} />);

storiesOf('Filter', module).add('Drawer', () => (
  <FilterMenu title={text('Title', 'Filter Title')} onApply={action('onApply')} content={FilterContentExpertise} />
));
