import React from 'react';
import { IntlProvider } from 'react-intl';
import Alert from './components/alert/Alert';

function App() {
  return (
    <IntlProvider>
      <Alert open>Hey!</Alert>
    </IntlProvider>
  );
}

export default App;
