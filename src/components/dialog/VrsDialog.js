import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { FormattedMessage } from 'react-intl';
import { Grid } from 'react-flexbox-grid';

const VrsDialog = ({ open, onClose, maxWidth, title, children, contentClass, dialogActions, ...otherProps }) => {
  return (
    <Dialog
      open={open || false}
      onClose={onClose}
      fullWidth
      classes={{ paper: 'manual-paper', container: 'modal-container' }}
      maxWidth={maxWidth || 'sm'}
      scroll="paper"
      {...otherProps}
    >
      <DialogTitle className="dialog-title" id="form-dialog-title">
        <FormattedMessage id={title}>{(txt) => <span className="modal-title">{txt}</span>}</FormattedMessage>
      </DialogTitle>
      <DialogContent>
        <Grid className={contentClass} fluid>
          {children}
        </Grid>
      </DialogContent>
      {dialogActions && <DialogActions classes={{ root: 'modal-footer' }}>{dialogActions}</DialogActions>}
    </Dialog>
  );
};

export default VrsDialog;
