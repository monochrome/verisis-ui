import React from 'react';
import { FormattedMessage } from 'react-intl';
import cls from 'classnames';
import styles from './NotificationAlert.module.scss';

const NotificationAlert = ({ icon, message, children, error, format, className, ...otherProps }) => {
  const Icon = icon;
  const { notificationAlert, danger } = styles;
  return (
    <div className={cls(notificationAlert, className, { [danger]: error })} {...otherProps}>
      {icon && <Icon />}
      {format ? <FormattedMessage id={message} /> : children}
    </div>
  );
};

export default NotificationAlert;
