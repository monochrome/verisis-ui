/* eslint-disable react/button-has-type */
import React from 'react';
import { FormattedMessage } from 'react-intl';
import cls from 'classnames';
import styles from './FabButton.module.scss';
import { isAuthorized } from '../util/Util';

const FabButton = ({ icon, onClick, background, type, disabled, danger, authorize, title, id }) => {
  const text = React.createRef();

  const ShowTooltip = () => {
    if (text.current) {
      text.current.style.display = 'flex';
    }
  };

  const HideTooltip = () => {
    if (text.current) {
      text.current.style.display = 'none';
    }
  };

  const element = (
    <div className={styles.fabContainer}>
      <button
        id={id}
        disabled={disabled}
        onClick={onClick}
        type={type || 'button'}
        className={cls(styles.fab, { [styles.danger]: danger })}
        style={{ background }}
        onMouseEnter={!disabled ? ShowTooltip : undefined}
        onMouseLeave={!disabled ? HideTooltip : undefined}
      >
        {icon}
      </button>
      {title && (
        <div className={styles.tooltip} ref={text} style={{ display: 'none' }}>
          <FormattedMessage id={title} />
        </div>
      )}
    </div>
  );

  if (authorize) {
    return isAuthorized({ id }) ? element : null;
  }
  return element;
};

export default FabButton;
