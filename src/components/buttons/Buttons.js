import React from 'react';
import cls from 'classnames';
import styles from './Buttons.module.scss';
import { isAuthorized } from '../util/Util';

const VrsButton = ({
  authorize,
  inlineStyle,
  id,
  children,
  className,
  icon,
  danger,
  link,
  hasBg,
  light,
  fullWidth,
  outlined,
  info,
  success,
  warning,
  small,
  large,
  noBorder,
  label,
  secondary,
  onlyIcon,
  ...otherProps
}) => {
  const Tag = link ? 'a' : 'button';
  const accents = [
    styles.button,
    {
      [styles.danger]: danger,
      [styles.hasBg]: hasBg,
      [styles.light]: light,
      ['light']: light,
      [styles.fullWidth]: fullWidth,
      [styles.onlyIcon]: !children,
      [styles.onlyIcon]: onlyIcon,
      [styles.outlined]: outlined,
      [styles.info]: info,
      [styles.success]: success,
      [styles.warning]: warning,
      [styles.large]: large,
      [styles.small]: small,
      [styles.noBorder]: noBorder,
      [styles.secondary]: secondary
    }
  ];
  const classes = cls(className, inlineStyle ? '' : accents);
  const element = (
    <Tag {...otherProps} className={classes}>
      {icon && icon}
      {children && children}
      {label && label}
    </Tag>
  );

  if (authorize) {
    return isAuthorized({ id }) ? element : null;
  }
  return element;
};

export default VrsButton;
