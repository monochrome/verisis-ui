import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Menu, MenuItem } from '@material-ui/core';
import styles from './SubButton.module.scss';
import { isAuthorized } from '../util/Util';
import cls from 'classnames';

class SubButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null
    };
  }

  handleClick = (event) => {
    const { dropdown, onClick } = this.props;
    console.log('??');
    if (dropdown) {
      this.setState({ anchorEl: event.currentTarget });
    } else {
      onClick();
    }
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { icon, label, id, item, dropdown, authorize, hasBg, onlyIcon, secondary, ...otherProps } = this.props;
    const { anchorEl } = this.state;
    const { subButton } = styles;
    const Icon = icon;
    const element = (
      <>
        <button
          id={id}
          onClick={this.handleClick}
          className={cls(
            subButton,
            { [styles.primary]: hasBg },
            { [styles.onlyIcon]: onlyIcon },
            { [styles.secondary]: secondary }
          )}
          {...otherProps}
        >
          {icon && Icon}
          {label && <FormattedMessage id={label} />}
        </button>
        {item && dropdown && (
          <Menu id={item.name} anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={this.handleClose}>
            {item.dropdown.map((menuItem, i) => (
              <MenuItem key={i.toString()} onClick={menuItem.action}>
                <FormattedMessage id={menuItem.name} />
              </MenuItem>
            ))}
          </Menu>
        )}
      </>
    );

    if (authorize) {
      return isAuthorized({ id }) ? element : null;
    }
    return element;
  }
}

export default SubButton;
