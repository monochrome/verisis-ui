import React, { Component } from 'react';
import { RadialBar, RadialBarChart, ResponsiveContainer } from 'recharts';
import { DefaultTheme } from './ChartColors';

const data = [
  { name: 'data1', count: 0, fill: DefaultTheme.chart_color_4 },
  { name: 'data2', count: 0, fill: DefaultTheme.chart_color_6 },
  { name: 'data3', count: 0, fill: DefaultTheme.chart_color_5 }
];

class RadialChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // activeIndex: 0,
    };
  }

  onPieEnter = () => {
    this.setState({
      // activeIndex: index,
    });
  };

  render() {
    return (
      <div className="chart-container">
        <span className="chart-title">Data Comparison by Type</span>
        <div className="chart">
          <ResponsiveContainer>
            <RadialBarChart
              cx="50%"
              cy="70%"
              innerRadius="50%"
              outerRadius="100%"
              data={data}
              startAngle={200}
              endAngle={-20}
              barSize={30}
            >
              <RadialBar
                minAngle={90}
                label={{ fill: '#666', position: 'insideStart' }}
                background
                clockWise
                dataKey="count"
              />
            </RadialBarChart>
          </ResponsiveContainer>
        </div>
      </div>
    );
  }
}

export default RadialChart;
