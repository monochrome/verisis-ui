import React from 'react';
import styles from './Charts.module.scss';
import VrsBarChart from './VrsBarChart';
import VrsPieChart from './VrsPieChart';

const Chart = ({ data, chartHeight, fromADJDProp, openHeader, type, onBarClick, dataColor, ...otherProps }) => {
  const { chartContainer, chartTitle, chart } = styles;
  if (data !== undefined) {
    return (
      <div className={chartContainer}>
        <h4 className={chartTitle}>{data.chartTitle}</h4>
        <div className={chart} style={{ direction: 'ltr' }}>
          {type === 'bar' && (
            <VrsBarChart
              data={data}
              chartHeight={chartHeight}
              onBarClick={onBarClick}
              fromADJDProp={fromADJDProp}
              openHeader={openHeader}
              dataColor={dataColor}
              styles={styles}
              {...otherProps}
            />
          )}
          {type === 'pie' && (
            <VrsPieChart
              styles={styles}
              data={data}
              dataColor={dataColor}
              chartHeight={chartHeight}
              fromADJDProp={fromADJDProp}
              {...otherProps}
            />
          )}
        </div>
      </div>
    );
  }
  return null;
};

export default Chart;
