import React from 'react';
import { Bar, BarChart, Legend, Tooltip, XAxis, YAxis, ResponsiveContainer, CartesianGrid, LabelList } from 'recharts';
import BarLegend from './BarLegend';
import TooltipContent from './TooltipContent';
import BarCustomizeLabel from './BarCustomizeLabel';

const VrsBarChart = ({ data, dataColor, chartHeight, onBarClick, fromADJDProp, styles, openHeader }) => {
  return (
    <ResponsiveContainer height={chartHeight || 290}>
      <BarChart maxBarSize={50} data={data.barData} margin={{ top: 0, right: 5, left: 5, bottom: 20 }}>
        <CartesianGrid strokeDasharray="5 5" stroke="#ddd" />
        <XAxis
          label={{ value: data.barchartXAxisLabel, position: 'bottom' }}
          dataKey={data.barChartDataKey}
          tickMargin={5}
          height={50}
          className={styles.axis}
        />

        <YAxis
          allowDecimals={false}
          label={{ value: data.barChartYAxisLabel, angle: -90, position: 'insideLeft' }}
          tickLine={false}
          axisLine={false}
          orientation="left"
          className={styles.axisColor}
        />

        <Tooltip isAnimationActive={false} content={TooltipContent} />

        <Legend
          verticalAlign="top"
          iconSize={11}
          iconType="circle"
          align="center"
          wrapperStyle={{
            color: '#b5bdce',
            fontSize: '.95em'
          }}
          height={50}
          content={BarLegend}
        />
        {dataColor.map((dataKey) => (
          <Bar
            key={dataKey.keyName}
            dataKey={dataKey.keyName}
            fill={dataKey.keyColor}
            onClick={onBarClick}
            isAnimationActive
          >
            {openHeader === true && fromADJDProp ? (
              <LabelList dataKey={dataKey.keyName} content={BarCustomizeLabel} />
            ) : null}
          </Bar>
        ))}
      </BarChart>
    </ResponsiveContainer>
  );
};

export default VrsBarChart;
