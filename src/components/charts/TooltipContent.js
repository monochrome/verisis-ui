import React from 'react';
import { FormattedMessage } from 'react-intl';
import styles from './Charts.module.scss';

const TooltipContent = ({ active, payload, label }) => {
  const { tooltip, tooltipTitle, tooltipContent, tooltipLabel, tooltipValue } = styles;
  if (active) {
    return (
      <div className={tooltip}>
        <h4 className={tooltipTitle}>{label}</h4>
        <ul className={tooltipContent}>
          {payload &&
            payload.map((item, i) => (
              <li key={i.toString()}>
                <strong className={tooltipLabel}>
                  <FormattedMessage id={item.dataKey} />:
                </strong>
                <span className={tooltipValue}>{item.value}</span>
              </li>
            ))}
        </ul>
      </div>
    );
  }

  return null;
};

export default TooltipContent;
