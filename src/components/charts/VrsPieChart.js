import React, { Component } from 'react';
import { Cell, Pie, PieChart, ResponsiveContainer } from 'recharts';
import PieActiveShape from './PieActiveShape';

class VrsPieChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeIndex: 0
    };
  }

  onPieEnter = (index) => {
    this.setState({
      activeIndex: index
    });
  };

  render() {
    const { activeIndex } = this.state;
    const { data, chartHeight, dataColor, fromADJDProp } = this.props;
    const active = Array.from(data.pieData);
    const activeIndexArr = active.map((i, index) => {
      return index;
    });
    let pieColor;
    if (dataColor && dataColor.length !== 0) {
      pieColor = dataColor;
    } else {
      pieColor = data.pieData;
    }

    return (
      <ResponsiveContainer height={chartHeight || 290}>
        <PieChart margin={{ bottom: 20 }}>
          <Pie
            activeIndex={fromADJDProp ? activeIndexArr : activeIndex}
            activeShape={PieActiveShape}
            isAnimationActive
            data={data.pieData}
            dataKey={data.pieDataKey}
            cx="50%"
            cy="50%"
            innerRadius="50%"
            outerRadius="70%"
          >
            {pieColor.map((item, index) => (
              <Cell key={index.toString()} fill={item.color} onMouseEnter={() => this.onPieEnter(index)} />
            ))}
          </Pie>
        </PieChart>
      </ResponsiveContainer>
    );
  }
}

export default VrsPieChart;
