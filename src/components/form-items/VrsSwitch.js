import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import Switch from '@material-ui/core/Switch';
import cls from 'classnames';
import styles from './VrsSwitch.module.scss';

class VrsSwitch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stateValue: props.checked ? props.checked : false
    };
  }

  handleChange = (value) => {
    const { onChange } = this.props;
    onChange(value);
    this.setState({
      stateValue: value
    });
  };

  render() {
    const { container, primaryColor, barColor, checkedBarColor, spanColor } = styles;
    const { checked, value, label, twoSide, onChange, icon, ...otherProps } = this.props;
    const { stateValue } = this.state;

    return (
      <div className={container} {...otherProps}>
        {twoSide && (
          <button
            onClick={() => this.handleChange(false)}
            className={cls(styles.labelButton, { [styles.active]: !stateValue })}
          >
            {label && <FormattedMessage id={label.left} />}
            {icon && icon.left}
          </button>
        )}
        <Switch
          classes={{
            colorPrimary: primaryColor,
            track: barColor,
            checked: checkedBarColor,
            thumb: spanColor
          }}
          checked={stateValue}
          onChange={(e, value) => this.handleChange(value)}
          value={value}
          disableRipple
        />
        {label && (
          <button
            onClick={() => this.handleChange(true)}
            className={cls(styles.labelButton, { [styles.active]: stateValue })}
          >
            {label && <FormattedMessage id={twoSide ? label.right : label} />}
            {icon && icon.right}
          </button>
        )}
      </div>
    );
  }
}

export default VrsSwitch;
