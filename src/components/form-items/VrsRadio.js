import React from 'react';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const VrsRadio = ({ nameHolder, name, value, string, onChange }) => {
  return (
    <Radio
      checked={nameHolder === value || false}
      onChange={(e) => onChange(name)(string ? e.target.value.toString() : parseInt(e.target.value, 10))}
      value={value}
      name={name}
      aria-label={name}
    />
  );
};

export const InlineRadio = ({ value, label, placement }) => {
  return <FormControlLabel value={value} control={<Radio />} label={label} labelPlacement={placement || 'end'} />;
};

export default VrsRadio;
