import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import { FormattedMessage } from 'react-intl';
import { isAuthorized } from '../util/Util';

class VrsTextField extends Component {
  state = {
    changed: false
  };

  handleChange = (e) => {
    const { onChange, isNumber } = this.props;

    if (isNumber) {
      const numberReg = new RegExp('^[0-9]+$');
      const isValid = numberReg.test(e.currentTarget.value);
      if (isValid || e.currentTarget.value === '') onChange(e.currentTarget.value);
    }

    if (onChange && !isNumber) onChange(e.currentTarget.value);
    this.setState({
      changed: true
    });
  };

  render() {
    const {
      id,
      type,
      label,
      value,
      placeholder,
      disabled,
      multiline,
      rowsMax,
      rows,
      style,
      children,
      formControlStyle,
      required,
      accept,
      isMultiFile,
      onBlur,
      variant,
      childRef,
      shouldAuthorize,
      onChange,
      maxLen,
      margin,
      noLabel,
      ...otherProps
    } = this.props;

    const { changed } = this.state;

    const InputLabelProps = {
      classes: {
        root: 'text-field-label',
        shrink: 'text-field-shrink'
      }
    };

    const InputProps = {
      classes: {
        root: 'text-field-input-root text-field-input',
        underline: 'text-field-underline'
      }
    };

    return (
      (!shouldAuthorize || isAuthorized(id)) && (
        <FormControl style={formControlStyle} margin={margin || 'normal'} fullWidth autoComplete="off">
          {noLabel ? (
            <Input
              name={id}
              id={id}
              inputRef={childRef}
              type={type}
              required={required}
              label={label}
              value={value || ''}
              error={required && changed && !value}
              placeholder={placeholder}
              onChange={this.handleChange}
              onBlur={onBlur ? (e) => onBlur(e.currentTarget.value) : null}
              disabled={disabled}
              className="text-field"
              multiline={multiline}
              rowsMax={rowsMax}
              rows={rows}
              variant={variant || 'standard'}
              autoComplete="off"
              // shouldUpdate={this.props.shouldUpdate || false}
              inputProps={{ 'aria-label': 'description' }}
              InputLabelProps={{ ...InputLabelProps }}
              style={style}
              {...otherProps}
            />
          ) : (
            <TextField
              name={id}
              id={id}
              inputRef={childRef}
              type={type}
              required={required}
              label={label}
              value={value || ''}
              error={required && changed && !value}
              placeholder={placeholder}
              onChange={this.handleChange}
              onBlur={onBlur ? (e) => onBlur(e.currentTarget.value) : null}
              disabled={disabled}
              className="text-field"
              multiline={multiline}
              rowsMax={rowsMax}
              rows={rows}
              variant={variant || 'standard'}
              autoComplete="off"
              // shouldUpdate={this.props.shouldUpdate || false}
              InputLabelProps={{ ...InputLabelProps }}
              InputProps={{ ...InputProps }}
              style={style}
              {...otherProps}
            />
          )}

          {children}
          {required && changed && !value && (
            <FormHelperText className="validation-error-message">
              <FormattedMessage id="isRequired" />
            </FormHelperText>
          )}
        </FormControl>
      )
    );
  }
}

export default VrsTextField;
