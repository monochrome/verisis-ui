import React, { Component } from 'react';
import { CheckCircleOutline, CheckCircle } from '@material-ui/icons';
import { Checkbox } from '@material-ui/core';
import cls from 'classnames';
import styles from './VrsCheckbox.module.scss';
import { isAuthorized } from '../util/Util';

class VrsCheckbox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stateChecked: false
    };
  }

  componentDidUpdate(prevProps) {
    const { checked } = this.props;
    const { stateChecked } = this.state;

    if (checked !== stateChecked) {
      this.setState({
        stateChecked: checked
      });
    }
  }

  handleClick = (isChecked) => {
    const { onChange } = this.props;
    if (onChange) {
      this.setState((oldState) => ({
        stateChecked: !oldState.stateChecked
      }));
      onChange(!isChecked);
    } else {
      this.setState((oldState) => ({
        stateChecked: !oldState.stateChecked
      }));
    }
  };

  render() {
    const {
      id,
      authorize,
      isRight,
      label,
      value,
      inline,
      className,
      checkboxClass,
      checkedIcon,
      radial,
      icon
    } = this.props;
    const { stateChecked } = this.state;
    const { checkboxBlock, fieldName, checkbox, checked } = styles;

    const element = (
      <div className={cls(className, { [checkboxBlock]: !inline })}>
        {!isRight && label && <span className={fieldName}>{label}</span>}
        <Checkbox
          checked={value || stateChecked}
          value={value || undefined}
          classes={{ checked }}
          onClick={() => this.handleClick(value || stateChecked)}
          className={cls(checkbox, checkboxClass)}
          checkedIcon={radial ? <CheckCircle /> : checkedIcon}
          icon={radial ? <CheckCircleOutline /> : icon}
        />
        {isRight && label && <span className={fieldName}>{label}</span>}
      </div>
    );

    if (authorize) {
      return isAuthorized({ id }) ? element : null;
    }
    return element;
  }
}

export default VrsCheckbox;
