import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class VrsForm extends Component {
  state = {
    isValid: true
  };

  componentWillMount() {
    this.timer = null;
  }

  onSubmit = async (e) => {
    const { isValid } = this.state;
    const { onSubmit, onInvalidSubmit } = this.props;
    e.preventDefault();

    await this.checkValidity();
    if (isValid) {
      return onSubmit && onSubmit();
    }
    return onInvalidSubmit && onInvalidSubmit();
  };

  onBlur = async (e) => {
    const { isValid } = this.state;
    const { onValid, onInvalid } = this.props;
    e.preventDefault();

    await this.checkValidity();
    if (isValid) {
      return onValid && onValid();
    }
    return onInvalid && onInvalid();
  };

  onChange = (e) => {
    e.preventDefault();

    const WAIT_INTERVAL = 500;

    clearTimeout(this.timer);

    this.timer = setTimeout(async () => {
      const { isValid } = this.state;
      const { onValid, onInvalid } = this.props;
      await this.checkValidity();
      if (isValid) {
        return onValid && onValid();
      }
      return onInvalid && onInvalid();
    }, WAIT_INTERVAL);
  };

  checkValidity = () => {
    const form = document.querySelector('form');
    const inputList = [...form.querySelectorAll('input')];

    if (inputList.length > 0) {
      const isInvalidExist = inputList.findIndex((input) => input.validity.valid === false);
      if (isInvalidExist === -1) {
        this.setState({
          isValid: true
        });
      } else {
        this.setState({
          isValid: false
        });
      }
    }
  };

  render() {
    const { children, style, className } = this.props;
    return children ? (
      <form style={style} className={className} onChange={this.onChange} onBlur={this.onBlur} onSubmit={this.onSubmit}>
        {children}
      </form>
    ) : (
      <span />
    );
  }
}

export default withRouter(VrsForm);
