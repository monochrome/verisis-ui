import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './SearchBox.module.scss';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';
import { Input, FormControl, InputAdornment, InputLabel } from '@material-ui/core';
import Search from '@material-ui/icons/Search';
import cls from 'classnames';

class SearchBox extends Component {
  handleSearchTermChange = (e) => {
    const { search } = this.props;
    search(e.currentTarget.value);
  };

  render() {
    const { intl, inline, centered } = this.props;
    return (
      <FormControl
        margin={inline ? 'normal' : 'none'}
        fullWidth
        autoComplete="off"
        className={cls(styles.searchBox, { [styles.centered]: centered })}
      >
        {inline && (
          <InputLabel htmlFor="search-box">
            <FormattedMessage id="search" />
          </InputLabel>
        )}
        <Input
          id="search-box"
          placeholder={!inline ? intl.messages.searchPlaceholder : ''}
          onChange={this.handleSearchTermChange}
          disableUnderline={!inline}
          className={styles.input}
          startAdornment={
            <InputAdornment position="start">
              <Search className={styles.icon} />
            </InputAdornment>
          }
        />
      </FormControl>
    );
  }
}

SearchBox.propTypes = {
  intl: intlShape,
  search: PropTypes.func
};

SearchBox.defaultProps = {
  intl: 'Search',
  search: ''
};

export default injectIntl(SearchBox);
