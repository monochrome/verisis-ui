import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import CreatableSelect from 'react-select/lib/Creatable';
import { FormHelperText } from '@material-ui/core';
import Select from 'react-select';
import './VrsSelectField.scss';
import { getCurrentLanguage } from '../util/Util';
import { Control, NoOptionsMessage, Option, MultiValue, ValueContainer, IndicatorsContainer } from './SelectFieldParts';

const components = {
  Control,
  MultiValue,
  NoOptionsMessage,
  Option,
  ValueContainer,
  IndicatorsContainer
};

class VrsSelectField extends Component {
  constructor(props) {
    super(props);

    this.state = {
      changed: false,
      focused: false
    };
  }

  handleChange = (value) => {
    const { onChange } = this.props;
    this.setState({
      changed: true
    });

    if (onChange) {
      if (value) {
        onChange(value);
      } else {
        onChange('');
      }
    }
  };

  newClick = () => {
    const { showModal } = this.props;
    showModal();
  };

  render() {
    const {
      label,
      value,
      data,
      required,
      isMulti,
      isCreatable,
      disabled,
      menuIsOpen,
      menuPosition,
      id,
      isOptionDisabled,
      menuPlacement,
      onCreateOption,
      isCopyable,
      isClearable,
      menuPortalTarget,
      margin,
      onBlur,
      onFocus
    } = this.props;
    const { changed, focused } = this.state;

    const selectStyles = {
      dropdownIndicator: (base) => ({
        ...base,
        padding: 5
      }),
      menuPortal: (base) => ({
        ...base,
        zIndex: 9999
      }),
      clearIndicator: (base) => ({
        ...base,
        padding: '0 8px'
      })
    };

    const currentLanguage = getCurrentLanguage();
    let newOptionMessage = 'Create new option';

    if (currentLanguage === 'ar') {
      newOptionMessage = 'إنشاء خيار جديد';
    } else if (currentLanguage === 'tr') {
      newOptionMessage = 'Yeni Seçenek Oluştur';
    }
    const Tag = isCreatable ? CreatableSelect : Select;
    return (
      <>
        <Tag
          styles={selectStyles}
          label={label}
          disabled={disabled}
          isDisabled={disabled}
          required={required}
          margin={margin}
          menuIsOpen={menuIsOpen}
          id={id}
          name={id}
          focused={focused}
          options={data}
          onFocus={() => {
            this.setState({ focused: true });
            if (onFocus) onFocus();
          }}
          onBlur={() => {
            this.setState({ focused: false });
            if (onBlur) onBlur();
          }}
          components={components}
          value={value || null}
          menuPosition={menuPosition || 'fixed'}
          onChange={(option) => this.handleChange(option, isMulti)}
          placeholder=""
          formatCreateLabel={(txt) => `${newOptionMessage}: ${txt}`}
          tabSelectsValue={false}
          isMulti={isMulti}
          menuPlacement={menuPlacement || 'auto'}
          closeMenuOnSelect={!isMulti}
          menuPortalTarget={menuPortalTarget}
          isOptionDisabled={isOptionDisabled}
          classNamePrefix="vrs"
          isCopyable={isCopyable || false}
          isClearable={isClearable || true}
          openMenuOnFocus
          menuShouldBlockScroll
          onCreateOption={onCreateOption}
          hideSelectedOptions={false}
        />

        {required && changed && !value && (
          <FormHelperText className="validation-error-message">
            <FormattedMessage id="isRequired" />
          </FormHelperText>
        )}
      </>
    );
  }
}

export default VrsSelectField;
