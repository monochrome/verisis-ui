import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { DatePicker, MuiPickersUtilsProvider, DateTimePicker } from '@material-ui/pickers';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import AccessTime from '@material-ui/icons/AccessTime';
import DateRange from '@material-ui/icons/DateRange';
import FormControl from '@material-ui/core/FormControl';
import moment from 'moment';
import 'moment/locale/ar-sa';
import 'moment/locale/tr';
import MomentUtils from '@date-io/moment';
import { getCurrentLanguage } from '../util/Util';

class VrsDatePicker extends PureComponent {
  currentLanguage = getCurrentLanguage();

  handleDateChange = (date) => {
    const { onChange } = this.props;
    if (onChange) {
      if (date !== null) {
        const modifiedDate = date.locale(this.currentLanguage).format('YYYY-MM-DDTHH:mm:ss');
        onChange(modifiedDate);
      } else {
        onChange(date);
      }
    }
  };

  render() {
    const {
      formControlStyle,
      style,
      required,
      label,
      value,
      minDate,
      maxDate,
      datetime,
      clearable,
      showTodayButton,
      disabled,
      id
    } = this.props;

    const dateFormat = 'DD/MM/YYYY';

    if (this.currentLanguage === 'ar') {
      moment.locale('en');
    } else if (this.currentLanguage === 'en') {
      moment.locale('en');
    } else {
      moment.locale('tr');
    }

    const commonProps = {
      rightArrowIcon: <ChevronRight />,
      leftArrowIcon: <ChevronLeft />,
      okLabel: <FormattedMessage id="choose" />,
      cancelLabel: <FormattedMessage id="abort" />,
      // todayLabel: <FormattedMessage id="today" />,
      clearLabel: <FormattedMessage id="clear" />,
      clearable: clearable || true,
      showTodayButton: showTodayButton || true,
      InputLabelProps: {
        classes: {
          root: 'text-field-label',
          shrink: 'text-field-shrink'
        }
      },
      InputProps: {
        classes: {
          root: 'text-field-input-root text-field-input',
          underline: 'text-field-underline'
        }
      },
      disabled,
      style,
      label,
      name: id,
      id
    };

    return (
      <MuiPickersUtilsProvider utils={MomentUtils} moment={moment}>
        <FormControl style={formControlStyle} margin="normal" fullWidth autoComplete="off">
          {!datetime ? (
            <DatePicker
              autoOk
              className="text-field"
              value={value || null}
              onChange={this.handleDateChange}
              required={required}
              minDate={minDate}
              maxDate={maxDate}
              format={dateFormat}
              {...commonProps}
            />
          ) : (
            <DateTimePicker
              className="text-field"
              ampm={false}
              value={value || null}
              onChange={this.handleDateChange}
              timeIcon={<AccessTime />}
              dateRangeIcon={<DateRange />}
              format={this.currentLanguage === 'ar' ? `${dateFormat} HH:mm` : `HH:mm ${dateFormat}`}
              {...commonProps}
            />
          )}
        </FormControl>
      </MuiPickersUtilsProvider>
    );
  }
}

export default VrsDatePicker;
