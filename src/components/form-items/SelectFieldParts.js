import React from 'react';
import { FormattedMessage } from 'react-intl';
import cls from 'classnames';
import MenuItem from '@material-ui/core/MenuItem';
import Check from '@material-ui/icons/Check';
import Chip from '@material-ui/core/Chip';
import CancelIcon from '@material-ui/icons/Cancel';
import FileCopy from '@material-ui/icons/FileCopy';
import styles from './SelectFieldsParts.module.scss';
import VrsTextField from './VrsTextField';

export const NoOptionsMessage = ({ innerProps }) => {
  return (
    <div className={cls(styles.noOptionsMessage)} {...innerProps}>
      <FormattedMessage id="noResult" />
    </div>
  );
};

export const inputComponent = ({ inputRef, ...props }) => {
  return <div ref={inputRef} {...props} />;
};

export const Control = ({ selectProps, innerRef, children, isMulti, innerProps, getValue }) => {
  const value = getValue();
  const { label, disabled, required, focused, margin } = selectProps;
  return (
    <VrsTextField
      label={label}
      disabled={disabled}
      required={required}
      margin={margin || 'normal'}
      InputProps={{
        inputComponent,
        classes: {
          underline: 'text-field-underline'
        },
        inputProps: {
          className: `${styles.input} text-field-input`,
          inputRef: innerRef,
          multiple: isMulti,
          value,
          children,
          ...innerProps
        }
      }}
      InputLabelProps={{
        shrink: (value && value.length !== 0) || focused,
        classes: {
          root: 'text-field-label',
          shrink: 'text-field-shrink'
        }
      }}
    />
  );
};

export const Option = ({ innerRef, isFocused, isSelected, isDisabled, innerProps, children }) => {
  const { menuItem, selected, optionContent, disabled } = styles;
  return (
    <MenuItem
      buttonRef={innerRef}
      selected={isFocused}
      component="div"
      className={cls(menuItem, { [selected]: isSelected, [disabled]: isDisabled })}
      {...innerProps}
    >
      <span className={optionContent}>
        {children}
        {isSelected && <Check />}
      </span>
    </MenuItem>
  );
};

export const ValueContainer = ({ children }) => {
  return <div className={styles.valueContainer}>{children}</div>;
};

export const MultiValue = ({ children, removeProps, isFocused }) => {
  const { chip, chipFocused } = styles;
  const handleCopy = (e) => {
    navigator.clipboard.writeText(e.target.innerText);
  };

  return (
    <Chip
      tabIndex={-1}
      label={children}
      className={cls(chip, { [chipFocused]: isFocused })}
      onClick={(e) => handleCopy(e)}
      onDelete={removeProps.onClick}
      deleteIcon={<CancelIcon {...removeProps} style={{ fontSize: '16px' }} />}
    />
  );
};

export const IndicatorsContainer = ({ children, hasValue, selectProps, getValue }) => {
  const { indicatorButton, focused } = styles;
  const handleCopy = () => {
    const value = getValue()[0];
    navigator.clipboard.writeText(value.value);
  };

  return (
    <div className="selectfield-indicators" style={{ display: 'flex', alignItems: 'center' }}>
      {selectProps.isCopyable && hasValue && !selectProps.isMulti && (
        <div
          className={cls(indicatorButton, { [focused]: selectProps.focused })}
          onClick={handleCopy}
          aria-hidden="true"
        >
          <FileCopy fontSize="small" />
        </div>
      )}
      {children}
    </div>
  );
};
