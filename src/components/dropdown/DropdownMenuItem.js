import React from 'react';
import cls from 'classnames';
import { FormattedMessage } from 'react-intl';
import styles from './DropdownMenuItem.module.scss';

const DropdownMenuItem = (props) => {
  const { itemName, onClick, danger, noTranslate, isActive, children, inImage } = props;
  return (
    // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
    <li
      className={cls(styles.dropdownMenuItem, {
        [styles.isActive]: isActive,
        [styles.danger]: danger,
        [styles.inImage]: inImage
      })}
      onKeyDown={onClick}
      onClick={onClick}
    >
      {noTranslate ? itemName : <FormattedMessage id={itemName} />}
      {children}
    </li>
  );
};

export default DropdownMenuItem;
