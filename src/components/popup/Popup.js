import React from 'react';
import cls from 'classnames';
import styles from './Popup.module.scss';

const Popup = ({ open, className, children, ...otherProps }) => {
  if (open) {
    return (
      <div className={cls(styles.popup, className)} {...otherProps}>
        {children}
      </div>
    );
  }
  return null;
};

export default Popup;
