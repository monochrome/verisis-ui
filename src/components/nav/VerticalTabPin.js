import React from 'react';
import styles from './VerticalTabPin.module.scss';

const VerticalTabPin = (props) => {
  const { children, onClick } = props;
  return (
    <button type="button" onClick={onClick} className={styles.verticalTabButton}>
      {children}
    </button>
  );
};

export default VerticalTabPin;
