import { lighten } from 'polished';

/* TODO: use sass instead of this. */
export const colors = {
  primaryColor: '#986f2f',
  primaryColorl10: lighten(0.1, '#986f2f'),
  primaryColorl20: lighten(0.2, '#986f2f'),
  primaryColorl30: lighten(0.3, '#986f2f'),
  primaryColorl40: lighten(0.4, '#986f2f'),
  sidebarLight: '#7f776b',
  sidebarDark: '#563f19',
  tActive: 'rgba(215, 181, 179, 0.1)',
  expertiseDarkBg: '#f9f7f4',
  danger: '#ff5c5c'
};
