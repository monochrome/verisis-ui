import React, { Component } from 'react';
import WebCam from 'react-webcam';
import { Camera, ArrowBack } from '@material-ui/icons';
import styles from './WebCamDialog.module.scss';
import VrsButton from '../buttons/Buttons';

class WebCamDialog extends Component {
  constructor(props) {
    super(props);
    this.wrapper = React.createRef();
  }

  componentDidMount() {
    document.addEventListener('click', this.handleOutsideClick);
  }

  setRef = (webcam) => {
    this.webcam = webcam;
  };

  takePicture = () => {
    const imageSrc = this.webcam.getScreenshot();
    const { onTakePicture } = this.props;

    if (onTakePicture) {
      onTakePicture(imageSrc);
    }
  };

  handleOutsideClick = (e) => {
    const { onClose } = this.props;
    if (this.wrapper.current && !this.wrapper.current.contains(e.target)) {
      onClose();
      document.removeEventListener('click', this.handleOutsideClick);
    }
  };

  render() {
    const { onClose } = this.props;
    const videoConstraints = {
      width: 1280,
      height: 720
    };
    return (
      <div className={styles.overlay} role="presentation">
        <div className={styles.webcamWrapper} ref={this.wrapper}>
          <div className={styles.webcam}>
            <WebCam
              ref={this.setRef}
              screenshotFormat="image/jpeg"
              width={1280}
              height={720}
              videoConstraints={videoConstraints}
            />
          </div>
          <div className={styles.buttons}>
            <VrsButton type="button" className={styles.button} onClick={onClose} icon={<ArrowBack />} hasBg />
            <VrsButton type="button" className={styles.button} onClick={this.takePicture} icon={<Camera />} hasBg />
          </div>
        </div>
      </div>
    );
  }
}

export default WebCamDialog;
