import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cls from 'classnames';
import styles from './PatternLock.module.scss';
import { colors } from '../colors';

const getDistance = (p1, p2) => Math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2);
const getAngle = (p1, p2) => Math.atan2(p2.y - p1.y, p2.x - p1.x);

class PatternLock extends PureComponent {
  static getPositionFromEvent({ clientX, clientY, touches }) {
    if (touches && touches.length) return { x: touches[0].clientX, y: touches[0].clientY };
    return { x: clientX, y: clientY };
  }

  constructor(props) {
    super(props);

    const points = [];
    for (let i = props.size ** 2 - 1; i >= 0; i -= 1) points.push({ x: 0, y: 0 });

    const frozen = props.path.length && props.freeze;

    this.state = {
      height: 0,
      path: frozen ? props.path : [],
      position: { x: 0, y: 0 },
      points,
      isFrozen: frozen
    };

    this.left = 0;
    this.top = 0;
    this.wrapper = React.createRef();
  }

  componentDidMount() {
    const { isFrozen } = this.state;
    this.updateHeight();
    if (isFrozen) {
      this.onChange();
    }
    this.wrapper.current.addEventListener('mouseup', this.onRelease);
    this.wrapper.current.addEventListener('touchend', this.onRelease);
  }

  componentDidUpdate(prevProps) {
    const { size, path } = this.props;
    if (prevProps.size !== size) {
      this.generatePoints();
    }

    if (prevProps.path !== path) {
      this.setPath(path);
    }
  }

  setPath = (path) => {
    this.setState({
      path
    });
  };

  generatePoints = () => {
    const { size } = this.props;

    this.setState({
      points: []
    });

    const points = [];

    for (let i = size ** 2 - 1; i >= 0; i -= 1) {
      points.push({ x: 0, y: 0 });
    }

    this.setState({
      points
    });
  };

  onHold = (e) => {
    const { disabled } = this.props;
    if (!disabled) {
      this.updateProperties();
      this.wrapper.current.addEventListener('mousemove', this.onMove);
      this.wrapper.current.addEventListener('touchmove', this.onMove);
      this.reset();
      this.updateMousePositionAndCheckCollision(e, true);
    }
  };

  onRelease = () => {
    const { path } = this.state;
    const { disabled } = this.props;
    console.log('hey');
    if (this.wrapper.current) {
      this.wrapper.current.removeEventListener('mousemove', this.onMove);
      this.wrapper.current.removeEventListener('touchmove', this.onMove);
    }

    if (!disabled && path.length > 0) {
      this.onChange();
    }
  };

  onChange = () => {
    const { onChange } = this.props;
    const { path } = this.state;
    this.setState({ isFrozen: true });
    onChange(path);
  };

  onMove = (e) => {
    this.updateMousePositionAndCheckCollision(e);
  };

  getExactPointPosition = ({ x, y }) => {
    const { pointActiveSize, connectorWidth } = this.props;
    const halfActiveSize = Math.floor(pointActiveSize / 2);
    const halfConnectorWidth = Math.floor(connectorWidth / 2);
    return {
      x: x + halfActiveSize,
      y: y + halfActiveSize - halfConnectorWidth
    };
  };

  getColor = (defaultColor, isActive = true) => {
    const { isFrozen } = this.state;
    const { freezeColor, disabledColor, disabled } = this.props;
    if (isFrozen && isActive) return freezeColor;
    if (disabled) return disabledColor;
    return defaultColor;
  };

  updateMousePositionAndCheckCollision = (e, reset) => {
    const { path } = this.state;
    const { x, y } = PatternLock.getPositionFromEvent(e);
    const position = { x: x - this.left, y: y - this.top };

    this.setState(
      {
        path: reset ? [] : path,
        position
      },
      this.detectCollision.bind(this, position)
    );
  };

  activate = (i) => {
    const { allowJumping, onDotConnect } = this.props;
    // eslint-disable-next-line react/destructuring-assignment
    let path = [...this.state.path];
    if (!allowJumping) {
      // eslint-disable-next-line react/destructuring-assignment
      path = [...path, ...this.checkJumping(this.state.path[this.state.path.length - 1], i)];
    }
    path.push(i);
    // console.log(i);
    this.setState({ path });
    if (onDotConnect) {
      onDotConnect(i);
    }
  };

  detectCollision = ({ x, y }) => {
    const { pointActiveSize, allowOverlapping } = this.props;
    const { path, points } = this.state;

    points.forEach((point, i) => {
      if ((allowOverlapping && path[path.length - 1] !== i) || path.indexOf(i) === -1) {
        if (x > point.x && x < point.x + pointActiveSize && y > point.y && y < point.y + pointActiveSize) {
          this.activate(i);
        }
      }
    });
  };

  checkJumping = (prev, next) => {
    const { size } = this.props;
    const { path } = this.state;

    const x1 = prev % size;
    const y1 = Math.floor(prev / size);

    const x2 = next % size;
    const y2 = Math.floor(next / size);

    if (y1 === y2) {
      // Horizontal
      const xDifference = Math.abs(x1 - x2);
      if (xDifference > 1) {
        const points = [];
        const min = Math.min(x1, x2);
        for (let i = 1; i < xDifference; i += 1) {
          const point = y1 * size + i + min;
          if (path.indexOf(point) === -1) points.push(point);
        }
        return points;
      }
    } else if (x1 === x2) {
      // Vertical
      const yDifference = Math.abs(y1 - y2);
      if (yDifference > 1) {
        const points = [];
        const min = Math.min(y1, y2);
        for (let i = 1; i < yDifference; i += 1) {
          const point = (i + min) * size + x1;
          if (path.indexOf(point) === -1) points.push(point);
        }
        return points;
      }
    } else {
      // Diagonal
      const xDifference = Math.abs(x1 - x2);
      const yDifference = Math.abs(y1 - y2);
      if (xDifference === yDifference && xDifference > 1) {
        const dirX = x2 - x1 > 0 ? 1 : -1;
        const dirY = y2 - y1 > 0 ? 1 : -1;
        const points = [];
        for (let i = 1; i < yDifference; i += 1) {
          const point = (i * dirY + y1) * size + i * dirX + x1;
          if (path.indexOf(point) === -1) points.push(point);
        }
        return points;
      }
    }
    return [];
  };

  updateHeight = () => {
    const height = this.wrapper.current.offsetWidth;
    this.setState({ height }, this.updateProperties);
  };

  updateProperties = () => {
    const { pointActiveSize, size } = this.props;
    const { height, points } = this.state;
    const halfSize = pointActiveSize / 2;
    const { left, top } = this.wrapper.current.getBoundingClientRect();
    const sizePerItem = height / size;
    const halfSizePerItem = sizePerItem / 2;

    this.left = left;
    this.top = top;

    const newPoints = points.map((x, i) => ({
      x: sizePerItem * (i % size) + halfSizePerItem - halfSize,
      y: sizePerItem * Math.floor(i / size) + halfSizePerItem - halfSize
    }));

    this.setState({ points: newPoints });
  };

  reset = () => {
    this.setState({
      isFrozen: false,
      path: []
    });
  };

  renderConnectors = () => {
    const { path, isFrozen, position, points } = this.state;
    const { connectorWidth, connectorColor, connectorRoundedCorners, disabled } = this.props;
    return (
      <div className={styles.reactPatternLockConntectorWrapper}>
        {path.map((x, i, arr) => {
          const toMouse = arr[i + 1] === undefined;
          if (toMouse && (disabled || isFrozen)) return null;

          const fr = this.getExactPointPosition(points[x]);
          let to = null;
          if (toMouse) {
            to = {
              x: position.x,
              y: position.y - connectorWidth / 2
            };
          } else {
            to = this.getExactPointPosition(points[arr[i + 1]]);
          }
          return (
            <div
              className={styles.reactPatternLockConntector}
              key={`${x}-${arr[i + 1]}`}
              style={{
                background: this.getColor(connectorColor),
                transform: `rotate(${getAngle(fr, to)}rad)`,
                width: `${getDistance(fr, to)}px`,
                left: `${fr.x}px`,
                top: `${fr.y}px`,
                height: connectorWidth,
                borderRadius: connectorRoundedCorners ? Math.round(connectorWidth / 2) : 0
              }}
            />
          );
        })}
      </div>
    );
  };

  renderPoints = () => {
    const { pointSize, pointActiveSize, pointColor, size, noPop } = this.props;
    const { path, points } = this.state;

    return points.map((x, i) => {
      const activeIndex = path.indexOf(i);
      const isActive = activeIndex > -1;
      const orderNumber = isActive ? activeIndex + 1 : null;
      const percentPerItem = 100 / size;

      return (
        <div
          key={i.toString()}
          className={styles.reactPatternLockPointWrapper}
          style={{ width: `${percentPerItem}%`, height: `${percentPerItem}%`, flex: `1 0 ${percentPerItem}%` }}
        >
          <div className={styles.reactPatternLockPoint} style={{ width: pointActiveSize, height: pointActiveSize }}>
            <div
              className={cls({ active: isActive && !noPop })}
              style={{ minWidth: pointSize, minHeight: pointSize, background: this.getColor(pointColor, isActive) }}
              data-order-number={orderNumber}
            />
          </div>
        </div>
      );
    });
  };

  render() {
    const { height } = this.state;
    const { className, style, width, invisible, disabled } = this.props;
    return (
      <div
        ref={this.wrapper}
        className={cls(styles.patternWrapper, { disabled, className })}
        style={{ ...style, width, height }}
        onMouseDown={this.onHold}
        onTouchStart={this.onHold}
        role="presentation"
      >
        {!invisible && this.renderConnectors()}
        {this.renderPoints()}
      </div>
    );
  }
}

PatternLock.defaultProps = {
  size: 3,
  onChange: () => {},
  freezeColor: colors.primaryColor,
  pointColor: colors.primaryColorl40,
  pointSize: 15,
  pointActiveSize: 30,
  connectorWidth: 2,
  connectorColor: colors.primaryColor,
  connectorRoundedCorners: false,
  disabledColor: '#BBB',
  invisible: false,
  noPop: false,
  disabled: false,
  freeze: false,
  allowOverlapping: false,
  allowJumping: false
};

PatternLock.propTypes = {
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  path: PropTypes.array,
  size: PropTypes.number,
  onChange: PropTypes.func,
  onDotConnect: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object,
  freezeColor: PropTypes.string,
  pointColor: PropTypes.string,
  pointSize: PropTypes.number,
  pointActiveSize: PropTypes.number,
  connectorWidth: PropTypes.number,
  connectorColor: PropTypes.string,
  connectorRoundedCorners: PropTypes.bool,
  disabledColor: PropTypes.string,
  invisible: PropTypes.bool,
  noPop: PropTypes.bool,
  disabled: PropTypes.bool,
  freeze: PropTypes.bool,
  allowOverlapping: PropTypes.bool,
  allowJumping: PropTypes.bool
};

export default PatternLock;
