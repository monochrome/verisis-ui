import React from 'react';
import classnames from 'classnames';
import Popup from 'reactjs-popup';
import './TrGroup.scss';

const TrGroup = ({ children, className, data, dataChanged, ...rest }) => {
  const dataIsSuitable =
    data !== undefined &&
    data.reasonForNonSuitability !== undefined &&
    !data.isSuitableForFilteringOperation &&
    data.reasonForNonSuitability !== null;
  if (dataIsSuitable) {
    return (
      <div className={classnames('rt-tr-group', className)} role="rowgroup" {...rest}>
        <Popup
          trigger={children[0]}
          on="hover"
          position="top center"
          contentStyle={{
            background: 'rgba(0, 0, 0, 0.9)',
            color: '#fff',
            border: '0',
            padding: '10px',
            borderRadius: '.25em',
            width: 'auto'
          }}
          arrowStyle={{ background: 'rgba(0, 0, 0, 0.8)' }}
        >
          {data.reasonForNonSuitability}
        </Popup>
      </div>
    );
  }
  return (
    <div className={classnames('rt-tr-group', className)} role="rowgroup" {...rest}>
      {children}
    </div>
  );
};

export default TrGroup;
