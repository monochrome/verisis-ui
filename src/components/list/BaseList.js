import React, { Component } from 'react';
import ReactTable from 'react-table';
import { FormattedMessage } from 'react-intl';
import checkboxHOC from 'react-table/lib/hoc/selectTable';
import 'react-table/react-table.css';
import './BaseList.scss';
import Tbody from './Tbody';
import { colors } from '../colors';
import InputComp from './InputComp';
import TrGroup from './TrGroup';
import Td from './Td';
import TableActions from './action/TableActions';

const CheckboxTable = checkboxHOC(ReactTable);

class BaseList extends Component {
  constructor(props) {
    super(props);

    this.checkboxTable = React.createRef();

    this.state = {
      totalHeight: '450px',
      selectAll: false,
      selectionState: [],
      modifiedColumns: []
    };
  }

  componentDidMount() {
    this.modifyColumns();
    this.canvasHeighter();
    window.addEventListener('resize', () => {
      this.canvasHeighter();
    });
  }

  componentDidUpdate(prevProps) {
    const { data, columns, selection, tableActions } = this.props;
    if (prevProps.data !== data) {
      this.canvasHeighter();
    }

    if (prevProps.columns !== columns) {
      this.modifyColumns();
    }

    if (prevProps.tableActions !== tableActions) {
      this.modifyColumns();
    }

    if (prevProps.selection !== selection) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        selectionState: selection
      });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => {
      this.canvasHeighter();
    });
  }

  modifyColumns = () => {
    const { columns, tableActions } = this.props;

    const modifiedColumns = columns.map((column) => ({
      ...column,
      Header: <FormattedMessage id={column.Header} />
    }));

    const actionCell = {
      Cell: ({ row }) => <TableActions row={row} actionList={tableActions} />,
      Header: <FormattedMessage id="actions" />,
      className: 'action-layer',
      sortable: false,
      resizable: false,
      filterable: false,
      width: 120
    };

    if (tableActions !== undefined && tableActions.length !== 0) {
      modifiedColumns.push(actionCell);
    }

    this.setState({
      modifiedColumns
    });
  };

  canvasHeighter = () => {
    const { heighters, disableHeighters } = this.props;
    if (!disableHeighters) {
      const header = document.querySelector('header');
      const subheader = document.querySelector('.subheader');
      let sumOtherHeight = 0;

      if (heighters) {
        heighters.map((heighter) => {
          if (heighter !== null) {
            if (heighter.current) {
              sumOtherHeight += heighter.current.clientHeight;
            } else {
              sumOtherHeight += heighter.clientHeight;
            }
          }
          return sumOtherHeight;
        });
      }

      if (header && subheader) {
        const headerHeight = header.clientHeight;
        const subheaderHeight = subheader.clientHeight;
        // buradaki +2 sub header içindeki border için :)
        const totalHeight = window.innerHeight - (subheaderHeight + headerHeight + sumOtherHeight + 2);

        this.setState({
          totalHeight:
            window.innerHeight >= 640
              ? parseInt(totalHeight, 10)
              : parseInt(totalHeight + headerHeight + subheaderHeight, 10)
        });
      }
    }
  };

  toggleAll = () => {
    const { keyField, toggleAll } = this.props;
    // eslint-disable-next-line react/destructuring-assignment
    const selectAll = !this.props.selectAll;
    const selection = [];

    this.setState((oldState) => ({ selectAll: !oldState.selectAll }));

    if (selectAll) {
      const wrappedInstance = this.checkboxTable.current.getWrappedInstance();
      const currentRecords = wrappedInstance.getResolvedState().sortedData;
      currentRecords.forEach((item) => {
        if (item._original.isSuitableForFilteringOperation !== undefined) {
          if (item._original.isSuitableForFilteringOperation) {
            selection.push(item._original[keyField]);
          }
        } else {
          selection.push(item._original[keyField]);
        }
      });
    }
    this.setState({ selectionState: selection });
    return toggleAll(selection, selectAll);
  };

  toggleSelection = (key) => {
    const { toggleSelection, data, selection } = this.props;
    const { selectionState } = this.state;
    let selectionsState = [...selectionState];
    let selections = [...selection];

    const keyIndex = selection.indexOf(key);

    if (keyIndex >= 0) {
      selections = [...selections.slice(0, keyIndex), ...selections.slice(keyIndex + 1)];
      selectionsState = [...selections.slice(0, keyIndex), ...selections.slice(keyIndex + 1)];
    } else {
      selections.push(key);
      selectionsState.push(key);
    }

    if (data.length === selections.length) {
      this.setState({ selectAll: true });
    } else {
      this.setState({ selectAll: false });
    }
    this.setState({ selectionState: selectionsState });
    return toggleSelection(selections);
  };

  isSelected = (key) => {
    const { selection } = this.props;
    return selection.includes(key);
  };

  render() {
    const {
      keyField,
      columns,
      data,
      width,
      style,
      maxHeight,
      className,
      selectedRow,
      onMouseEnterTR,
      onMouseOutTR,
      onSortedChange,
      infiniteScroll,
      loadMore,
      isFetchDone,
      totalRecordCount,
      checkbox,
      SubComponent,
      sortable,
      disableAutomation,
      expanded,
      defaultExpanded,
      onExpandedChange,
      disableScroller
    } = this.props;
    const { totalHeight, selectionState, modifiedColumns } = this.state;

    const Tag = checkbox ? CheckboxTable : ReactTable;

    const dataIds = checkbox ? data && data.map((item) => item[keyField]) : '';

    const checkboxProps = {
      toggleSelection: this.toggleSelection,
      toggleAll: this.toggleAll,
      isSelected: this.isSelected,
      SelectAllInputComponent: InputComp,
      SelectInputComponent: InputComp,
      selectWidth: 70,
      selectType: 'checkbox',
      selectAll: data && data.length !== 0 && dataIds.toString() === selectionState.toString()
    };

    const trGroupProps = (s, r) => ({
      data: r !== undefined && r.original !== false ? r.original : undefined
    });

    const getTrProps = (s, r) => {
      const { keyField, onRowDblClick, onRowClick, filterOp, dataChanged } = this.props;
      let selected;
      let isDisabled;
      let backgroundColor;
      let borderTop;

      if (r !== undefined) {
        const { isSuitableForFilteringOperation, reasonForNonSuitability, isFirstRecord } = r.original;
        selected = checkbox && r.original[keyField] ? this.isSelected(r.original[keyField]) : false;
        isDisabled = filterOp && dataChanged && !isSuitableForFilteringOperation && reasonForNonSuitability !== null;

        if (isFirstRecord) {
          borderTop = '2px solid #000';
        }

        if (r.index === selectedRow || selected) {
          backgroundColor = colors.tActive;
        } else {
          backgroundColor = 'inherit';
        }
      }
      return {
        style: {
          borderTop,
          backgroundColor,
          color: isDisabled ? 'rgba(0, 0, 0, 0.26)' : 'inherit'
        },
        className: 'rt-tr',
        onDoubleClick: (e) => {
          const rowIdx = r.index;
          if (onRowDblClick && rowIdx !== -1) {
            onRowDblClick(rowIdx, r, e);
          }
        },
        onClick: (e) => {
          const rowIdx = r.index;
          if (onRowClick && rowIdx !== -1) {
            onRowClick(rowIdx, r, e);
          }
        },
        onMouseEnter: onMouseEnterTR,
        onMouseOut: onMouseOutTR
      };
    };

    const getTbodyProps = (state) => ({
      columnData: state.allVisibleColumns,
      resizedData: state.resized,
      disableScroller,
      infiniteScroll,
      loadMore,
      isFetchDone,
      totalRecordCount
    });

    return (
      <Tag
        manual
        keyField={keyField}
        ref={this.checkboxTable}
        showPagination={false}
        className={className}
        data={data}
        columns={!disableAutomation ? modifiedColumns : columns}
        resizable={false}
        style={style || { height: totalHeight, width, maxHeight }}
        multiSort={false}
        sortable={sortable || false}
        minRows={1}
        onSortedChange={onSortedChange}
        getTrGroupProps={trGroupProps}
        getTrProps={getTrProps}
        getTbodyProps={getTbodyProps}
        TbodyComponent={Tbody}
        TrGroupComponent={TrGroup}
        TdComponent={Td}
        SubComponent={SubComponent}
        expanded={expanded}
        defaultExpanded={defaultExpanded}
        onExpandedChange={onExpandedChange}
        noDataText={<FormattedMessage id="noDataText" />}
        {...(checkbox ? checkboxProps : undefined)}
      />
    );
  }
}

export default BaseList;
