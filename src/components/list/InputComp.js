import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';

const InputComp = ({ row, checked, onClick, id }) => {
  let isDisabled;

  if (row !== undefined && row.isSuitableForFilteringOperation !== undefined) {
    isDisabled = !row.isSuitableForFilteringOperation;
  } else {
    isDisabled = false;
  }

  return (
    <Checkbox
      checked={checked}
      disabled={isDisabled}
      onClick={(e) => {
        const { shiftKey } = e;
        e.stopPropagation();
        onClick(id, shiftKey, row);
      }}
      onChange={() => {}}
      color="default"
    />
  );
};

export default InputComp;
