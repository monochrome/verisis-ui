import React, { Component } from 'react';
import cls from 'classnames';
import Waypoint from 'react-waypoint';
import Check from '@material-ui/icons/Check';
import Refresh from '@material-ui/icons/Refresh';
import { FormattedMessage } from 'react-intl';
import styles from './Tbody.module.scss';
import { getCurrentLanguage } from '../util/Util';

class Tbody extends Component {
  constructor(props) {
    super(props);

    this.state = {
      width: 0
    };
  }

  componentDidMount() {
    this.setState({
      placeholder: 1
    });

    window.addEventListener('resize', () => this.calculateScroll());
  }

  componentDidUpdate(prevProps) {
    const { columnData, resizedData } = this.props;
    if (prevProps.columnData !== columnData) {
      this.widthizer(columnData);
    }

    if (prevProps.resizedData !== resizedData) {
      this.widthizer(resizedData, true);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('resize', () => this.calculateScroll());
  }

  widthizer = (data, resize) => {
    const columnWidths = [];

    if (data) {
      data.map((column) =>
        !column.width ? columnWidths.push(column.minWidth) : columnWidths.push(parseInt(column.width, 10))
      );
    }

    if (resize) {
      data.map((d) => (data.length ? columnWidths.push(Math.floor(d.value)) : 0));
    }

    let rowMinWidth;

    if (columnWidths.length !== 0) {
      rowMinWidth = columnWidths.reduce((acc, val) => acc + val);
      this.setState({
        width: rowMinWidth
      });
    }

    this.calculateScroll(rowMinWidth);
  };

  calculateScroll = (rowMinWidth) => {
    const currentLanguage = getCurrentLanguage();
    let { width } = this.state;
    const { disableScroller } = this.props;
    if (!disableScroller) {
      const tHead = document.querySelector('.rt-thead');
      const tBody = document.querySelector('.rt-tbody');
      const scrollWidth = tBody.offsetWidth - tBody.clientWidth;

      if (rowMinWidth) width = rowMinWidth;

      if (width < tBody.clientWidth) {
        if (currentLanguage === 'ar') {
          tHead.style.paddingLeft = `2px`;
        } else {
          tHead.style.paddingRight = `${scrollWidth}px`;
        }
      } else {
        tHead.style.paddingRight = '';
      }
    }
  };

  loadMore = (fetchStatus) => {
    const { loadMore } = this.props;
    if (!fetchStatus && loadMore) {
      loadMore();
    }
  };

  render() {
    const { children, infiniteScroll, isFetchDone } = this.props;
    const { placeholder, width } = this.state;

    return (
      <div className={cls('rt-tbody', styles.tBody)} style={{ minWidth: width }}>
        {children}
        {infiniteScroll && (
          <React.Fragment>
            <Waypoint
              onEnter={() => this.loadMore(isFetchDone)}
              scrollableAncestor={document.querySelector('rt-tbody')}
              placeholderState={placeholder}
            />
            <div className={isFetchDone ? styles.fetchDone : styles.loadMore}>
              {isFetchDone ? (
                <span className={styles.checkIcon}>
                  <Check />
                </span>
              ) : (
                <span className={styles.loadingIcon}>
                  <Refresh />
                </span>
              )}
              <FormattedMessage id={isFetchDone ? 'loadingisFinishText' : 'loadingText'} />
            </div>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default Tbody;
