import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import styles from './ListTotalCount.module.scss';
import Popup from '../popup/Popup';

class ListTotalCount extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isPopupOpen: false,
      hasPopup: false
    };

    this.content = React.createRef();
  }

  componentDidUpdate(prevProps) {
    const { caption } = this.props;
    if (prevProps.caption !== caption) {
      this.isOverflown(this.content.current);
    }
  }

  onMouseEvent = (type) => {
    if (type === 'over') {
      this.timer = setTimeout(() => {
        this.setState({
          isPopupOpen: true
        });
      }, 400);
    }

    if (type === 'out') {
      clearTimeout(this.timer);
      this.setState({
        isPopupOpen: false
      });
    }
  };

  isOverflown = (element) => {
    let hasScroll;

    if (element !== null) {
      hasScroll = element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth;
    }

    if (hasScroll) {
      this.setState({
        hasPopup: true
      });
    }
  };

  render() {
    const { count, totalCount, caption } = this.props;
    const { isPopupOpen, hasPopup } = this.state;
    const isListFull = count === totalCount;
    const recordTypeText = isListFull ? totalCount : `${count} / ${totalCount}`;
    return (
      <div className={styles.caseCount}>
        {count !== 0 && (
          <div className={styles.wrapperForCaption}>
            {!hasPopup && (
              <div ref={this.content} className={styles.captionBox}>
                {caption}:
              </div>
            )}
            {hasPopup && (
              <>
                <div
                  className={styles.captionBox}
                  onMouseEnter={() => this.onMouseEvent('over')}
                  onMouseLeave={() => this.onMouseEvent('out')}
                >
                  {caption}
                  {isPopupOpen && (
                    <Popup open={isPopupOpen} className={styles.popup}>
                      {caption && caption}
                      {!caption && <FormattedMessage id="noDataText" />}
                    </Popup>
                  )}
                </div>
                <div>:</div>
              </>
            )}
            <div className={styles.recordBox}>{recordTypeText}</div>
          </div>
        )}
      </div>
    );
  }
}

export default ListTotalCount;
