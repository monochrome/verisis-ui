import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Menu, MenuItem } from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import styles from './TableActions.module.scss';
import TableButton from './TableButton';

class TableActions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null
    };
  }

  handleClick = (event) => {
    const { actionList, row } = this.props;

    if (actionList.length > 4) {
      this.setState({ anchorEl: event.currentTarget });
    } else {
      actionList.forEach((actionItem) => {
        actionItem.action(row);
      });
    }
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { actionList, row } = this.props;
    const { anchorEl } = this.state;

    return actionList.length > 4 ? (
      <>
        <TableButton type="button" onClick={this.handleClick} className={styles.iconBox}>
          {
            <div className={styles.topNavIcon}>
              <MoreVert />
            </div>
          }
        </TableButton>
        <Menu
          id={row}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          className={styles.dropdownWrapper}
        >
          {actionList.map(
            (menuItem, i) =>
              console.log('menuitem', menuItem) || (
                <MenuItem key={i.toString()} onClick={() => menuItem.action(row)} className={styles.dropdownListItem}>
                  <div className={styles.dropdownListIcon}>{menuItem.icon}</div>
                  <FormattedMessage id={menuItem.title} />
                </MenuItem>
              )
          )}
        </Menu>
      </>
    ) : (
      <div className={`${styles.actionList} actionList `}>
        {actionList.map((actionItem, i) => {
          if (actionItem.icon) {
            return (
              <div className={`${styles.buttonContainer} buttonContainer`} key={i.toString()}>
                <TableButton title={actionItem.title} onClick={() => actionItem.action(row)}>
                  {typeof actionItem.icon === 'function' ? actionItem.icon(row) : actionItem.icon}
                </TableButton>
              </div>
            );
          }
          return null;
        })}
      </div>
    );
  }
}

export default TableActions;
