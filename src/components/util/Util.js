export const AUTH_KEY = 'userRoleAuthorizations';

const loaders = [];

export const loader = (isLoading) => {
  const loader = document.querySelector('#loader');
  loaders.push(isLoading);
  let trueCount = 0;
  let falseCount = 0;

  loaders.forEach((loader) => {
    if (loader) {
      trueCount += 1;
    } else {
      falseCount += 1;
    }
  });

  if (loader) {
    if (trueCount > falseCount) {
      loader.style.display = 'flex';
      loader.style.justifyContent = 'center';
      loader.style.alignItems = 'center';
    } else {
      loader.style.display = 'none';
    }
  }
};

export const roleAuthorizations = () => JSON.parse(sessionStorage.getItem(AUTH_KEY));

export const isAuthorized = (object, authorizationList) => {
  const authorization = authorizationList || roleAuthorizations();
  const returnValue = authorization.find(
    (item) => item.uiName === object.id || item.uiName === object.name || item.uiName === object
  );

  if (returnValue !== undefined) {
    return returnValue.isAuthorized;
  }

  return false;
};

export const getCurrentLanguage = (needMap, supportedLanguages) => {
  const languageCode = JSON.parse(localStorage.getItem('CurrentLanguage'))
    ? JSON.parse(localStorage.getItem('CurrentLanguage')).CurrentLanguage
    : 'en';

  if (needMap) {
    return supportedLanguages.find((language) => language.langCode === languageCode);
  }
  return languageCode;
};

export const authorizedFilter = (mainObj, filtObj) => {
  const filtered = mainObj.filter((items) => isAuthorized(items, filtObj));

  return filtered;
};
