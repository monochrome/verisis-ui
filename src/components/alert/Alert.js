import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import { Warning } from '@material-ui/icons';
import { FormattedMessage } from 'react-intl';
import ModalButton from '../buttons/ModalButton';
import styles from './Alert.module.scss';

class Alert extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stateOpen: false
    };
  }

  handleClickOpen = () => {
    this.setState({ stateOpen: true });
  };

  handleClose = () => {
    this.setState({ stateOpen: false });
  };

  render() {
    const {
      open,
      alertTitle,
      alertText,
      onCancelClick,
      onContinueClick,
      cancelButtonText,
      continueButtonText,
      maxWidth,
      warning
    } = this.props;

    const { stateOpen } = this.state;

    return (
      <div>
        <Dialog
          open={open || stateOpen}
          onClose={this.handleClose}
          fullWidth
          maxWidth={maxWidth || 'sm'}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <div className={styles.dialogHeader}>
            {warning && (
              <div className={styles.iconForAlert}>
                <Warning />
              </div>
            )}
            <DialogTitle id="alert-dialog-title">
              <FormattedMessage id={alertTitle || 'There is no alertTitle'} />
            </DialogTitle>
          </div>
          <DialogContent classes={{ root: styles.makeFlex }}>
            <DialogContentText id="alert-dialog-description">
              <FormattedMessage id={alertText || 'continueToAction'} />
            </DialogContentText>
          </DialogContent>
          <DialogActions style={{ padding: '1em' }}>
            <ModalButton onClick={onCancelClick || this.handleClose} label={cancelButtonText || 'cancel'} />
            <ModalButton onClick={onContinueClick || this.handleClose} label={continueButtonText || 'continue'} />
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default Alert;
